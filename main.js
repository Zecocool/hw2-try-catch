// Банально самого себе перестраховувати щоб не допустити синтаксичну помилку, 
// а в роботі це робота з сервером, трапляються такі випадки коли приходять не повні данні, 
// або з помилками.

const books = [

  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.getElementById("root");

const bookList = document.createElement("ul");

root.append(bookList);

const nameKeys = ["author", "name", "price"];

books.map((book, index) => {
  const listItem = document.createElement("li");
  try {
    for (const key of nameKeys) {
      if (!book[key]){
        throw (
          "Єлемент масиву books за номером " +
          index +
          " не має " +
          key +
          " "
        )
      }else{
        listItem.innerText += book[key] + " ";
    }
    }
    bookList.append(listItem);
  } catch (error) {
    console.error(error);
  }
});